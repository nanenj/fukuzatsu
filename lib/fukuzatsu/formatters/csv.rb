module Formatters

  class Csv

    include Formatters::Base

    def header
      columns.join(',')
    end

    def content
      if options[:file_index].nil? || options[:file_index] == 0 
        [header, rows, footer].flatten.join("\r\n")
      else
        [rows, footer].flatten.join("\r\n")
      end
    end

    def rows
      file.methods.inject([]) do |a, method|
        a << "#{self.filename},#{file.class_name},#{method.prefix}#{method.name},#{method.complexity}"
        a
      end.join("\r\n")
    end

    def filename
      file.path_to_file.split('/')[-1] 
    end

    def footer
    end

    def export
      begin
        if options[:file_index]
          outfile = File.open("results.csv", 'ab')
        else
          outfile = File.open("#{path_to_results}", 'w')
        end
        puts content.inspect
      outfile.write(content)
      rescue Exception => e
        puts "Unable to write output: #{e} #{e.backtrace}"
      ensure
        outfile && outfile.close
      end
    end

    def file_extension
      ".csv"
    end

  end

end